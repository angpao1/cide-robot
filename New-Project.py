import os
try:
    import xlsxwriter
except:
    os.system("pip install xlsxwriter==1.3.7")

# define the name of the directory to be created
TEST_DATA = "TEST_DATA"
TEST_KEYWORDS = "TEST_KEYWORDS"
TEST_RESULTS = "TEST_RESULTS"
TEST_SUITE = "TEST_SUITE"

folder = input('Project Name : ')
for x in TEST_DATA, TEST_KEYWORDS, TEST_RESULTS, TEST_SUITE:
    try:
        os.mkdir('{}/{}'.format(x,folder))
    except OSError:
        print ("Creation of the directory {}/{} failed".format(x,folder))
    else:
        print ("Successfully created the directory {}/{} ".format(x,folder))


# Create a workbook and add a worksheet.
# TEST_DATA
workbook = xlsxwriter.Workbook('TEST_DATA/{}/Example.xlsx'.format(folder))
worksheet = workbook.add_worksheet('DATA')
workbook.close()

# TEST_KEYWORDS
f = open('TEST_KEYWORDS/{}/KEYWORDS.txt'.format(folder), "w")
f.write('''*** Settings ***
Resource          ../AllSetting.txt

*** Keywords ***
TEST
    Log To Console    ROBOT
    ''')
f.close()

# TEST_KEYWORDS
f = open('TEST_KEYWORDS/AllSetting.txt', "r")
data = f.read()
if '{}'.format(folder) in data:
    print(data)
else:
    f = open('TEST_KEYWORDS/AllSetting.txt', "w")
    write_data = data.replace("Browsers.txt", "Browsers.txt\nResource          {}/KEYWORDS.txt".format(folder))
    f.write(write_data)
    f.close()
f.close()

# TEST_SUITE
f = open('TEST_SUITE/{}/TESTCASE.robot'.format(folder), "w")
f.write('''*** Settings ***
Resource          ../../TEST_KEYWORDS/AllSetting.txt

*** Test Cases ***
TEST CASES
    Open excel test data    ${TEST_DATA}    xxxx    Example.xlsx
    ${Count}    Count rows test data    xxxx
    FOR    ${i}    IN RANGE    2    ${Count}+1
        ${DATA}    Read row test data    xxxx    ${i}
        ${STARTTIME}    Get Current Date
        Run Keyword    Log To Console    TEST
        ${ENDTIME}    Get Current Date
        ${LIST_KEY}    Append to result title    RUNNING    STARTTIME    ENDTIME
        ${LIST_VALUE}    Append to result data    PASS    ${STARTTIME}    ${ENDTIME}
        Reports excel    ${TEST_RESULTS}    xxxx    ${LIST_KEY}    ${LIST_VALUE}    ${i}
    END
    Close excel test data    xxxx
    '''.replace('xxxx',folder))
f.close()

input('Success...')