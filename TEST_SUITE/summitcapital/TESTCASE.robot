*** Settings ***
Resource          ../../TEST_KEYWORDS/AllSetting.txt

*** Test Cases ***
TEST CASES
    Open excel test data    ${TEST_DATA}    summitcapital    summitcapital.xlsx
    ${Count}    Count rows test data    summitcapital
    FOR    ${i}    IN RANGE    2    ${Count}+1
        ${DATA}    Read row test data    summitcapital    ${i}
        ${STARTTIME}    Get Current Date
        Run Keyword If    '${DATA}[StatusRuning]' == 'Run'    ${DATA}[Testcase]    ${DATA}
        Comment    ${RETURN}    Actions    Summitcapital    ${DATA}
        ${ENDTIME}    Get Current Date
        ${LIST_KEY}    Append to result title    StatusRuning    TestName    STARTTIME    ENDTIME    TS_MESSAGE    Car_price    down_payment
        ${LIST_VALUE}    Append to result data    ${DATA}[StatusRuning]    ${DATA}[Testcase]    ${STARTTIME}    ${ENDTIME}    Fail    ${DATA}[Car_price]    ${DATA}[down_payment]
        Comment    ${LIST_VALUE}    Append to result data    ${RETURN}[0]    ${STARTTIME}    ${ENDTIME}    ${RETURN}[1]    ${DATA}[Car_price]    ${DATA}[down_payment]
        Reports excel    ${TEST_RESULTS}    summitcapital    ${LIST_KEY}    ${LIST_VALUE}    ${i}
    END
    Close excel test data    summitcapital

TEST CASES2
    Open excel test data    ${TEST_DATA}    summitcapital    summitcapital.xlsx
    ${Count}    Count rows test data    summitcapital
    FOR    ${i}    IN RANGE    2    ${Count}+1
        ${DATA}    Read row test data    summitcapital    ${i}
        ${STARTTIME}    Get Current Date
        Run Keyword If    '${DATA}[StatusRuning]' == 'Run'    ${DATA}[Testcase]    ${DATA}
        Comment    ${RETURN}    Actions    Summitcapital    ${DATA}
        ${ENDTIME}    Get Current Date
        ${LIST_KEY}    Append to result title    StatusRuning    TestName    STARTTIME    ENDTIME    TS_MESSAGE    Car_price    down_payment
        ${LIST_VALUE}    Append to result data    ${DATA}[StatusRuning]    ${DATA}[Testcase]    ${STARTTIME}    ${ENDTIME}    Fail    ${DATA}[Car_price]    ${DATA}[down_payment]
        Comment    ${LIST_VALUE}    Append to result data    ${RETURN}[0]    ${STARTTIME}    ${ENDTIME}    ${RETURN}[1]    ${DATA}[Car_price]    ${DATA}[down_payment]
        Reports excel    ${TEST_RESULTS}    summitcapital    ${LIST_KEY}    ${LIST_VALUE}    ${i}
    END
    Close excel test data    summitcapital