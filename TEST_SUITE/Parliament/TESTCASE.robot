*** Settings ***
Resource          ../../TEST_KEYWORDS/AllSetting.txt

*** Test Cases ***
TEST CASES
    Open excel test data    ${TEST_DATA}    Parliament    Example.xlsx
    ${Count}    Count rows test data    Parliament
    FOR    ${i}    IN RANGE    2    ${Count}+1
        ${DATA}    Read row test data    Parliament    ${i}
        ${STARTTIME}    Get Current Date
        ${RETURN}    Actions    STEP    ${DATA}
        ${ENDTIME}    Get Current Date
        ${LIST_KEY}    Append to result title    Rooms    RUNNING    STARTTIME    ENDTIME    MESSAGE
        ${LIST_VALUE}    Append to result data    ${DATA}[Room]    ${RETURN}[0]    ${STARTTIME}    ${ENDTIME}    ${RETURN}[1]
        Reports excel    ${TEST_RESULTS}    Parliament    ${LIST_KEY}    ${LIST_VALUE}    ${i}
    END
    Close excel test data    Parliament
